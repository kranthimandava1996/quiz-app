package com.example.techquizapp.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.techquizapp.R;
import com.example.techquizapp.db.DBAdapter;
import com.example.techquizapp.models.User;

public class ChangePassword extends AppCompatActivity {

    EditText old_password;
    EditText new_password;
    EditText confirm_password;
    String pwd;

    Button save;
    String email_id;
    Boolean check_is_changedPwd;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.change_password);
        old_password = (EditText) findViewById(R.id.oldPassword);
        new_password = (EditText) findViewById(R.id.newPassword);
        confirm_password = (EditText) findViewById(R.id.confirmPassword);
        email_id = getIntent().getStringExtra("email_id");
        save = (Button) findViewById(R.id.changePasswod);
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    DBAdapter dbAdapter = new DBAdapter(ChangePassword.this);
                    String opwd = old_password.getText().toString().trim();
                    String npwd = new_password.getText().toString().trim();
                    String cpwd = confirm_password.getText().toString().trim();
                    User user = dbAdapter.getUserInfo(email_id);
                    pwd = user.getPassword();

                    if (opwd.equals("")) {
                        old_password.setError("Required field.");
                    } else if (npwd.equals("")) {
                        new_password.setError("Required field.");
                    } else if (cpwd.equals("")) {
                        confirm_password.setError("Required field.");
                    } else {
                        if (npwd.equals(cpwd)) {
                            if (pwd.equals(opwd)) {
                                check_is_changedPwd = dbAdapter.changePassword(opwd, npwd, email_id);
                                if (check_is_changedPwd) {
                                    Toast.makeText(ChangePassword.this, "Password has been changed successfully.", Toast.LENGTH_SHORT).show();
                                    Intent intent = new Intent(ChangePassword.this, DashBoard.class);
                                    startActivity(intent);
                                    old_password.setText("");
                                    new_password.setText("");
                                    confirm_password.setText("");
                                } else {
                                    Toast.makeText(ChangePassword.this, "Something went wrong.", Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                Toast.makeText(ChangePassword.this, "Old password is not correct.", Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Toast.makeText(ChangePassword.this, "Passwords are not matched.", Toast.LENGTH_SHORT).show();
                        }
                    }
                } catch (Exception e) {
                    Toast.makeText(ChangePassword.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }
}
