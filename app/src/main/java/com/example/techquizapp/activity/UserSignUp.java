package com.example.techquizapp.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.techquizapp.R;
import com.example.techquizapp.db.DBAdapter;
import com.example.techquizapp.models.User;

public class UserSignUp extends AppCompatActivity {
    private EditText firstname;
    private EditText lastname;
    private EditText email_id;
    private EditText mobile_number;
    private EditText password;
    private EditText confirm_password;
    DBAdapter dbAdapter;
    Boolean check_insert_data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.user_new_account);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Button submit = (Button) findViewById(R.id.submit);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                firstname = (EditText) findViewById(R.id.editTextFirstName);
                lastname = (EditText) findViewById(R.id.editTextLastName);
                email_id = (EditText) findViewById(R.id.editTextEmailId);
                mobile_number = (EditText) findViewById(R.id.editTextMoNo);
                password = (EditText) findViewById(R.id.editTextPassword);
                confirm_password = (EditText) findViewById(R.id.editTextConfirmPassword);

                String fname = firstname.getText().toString();
                String lname = lastname.getText().toString();
                String emailid = email_id.getText().toString();
                String mobilenumber = mobile_number.getText().toString();
                String pwd = password.getText().toString().trim();
                String confirmpassword = confirm_password.getText().toString().trim();

                User user = new User();
                user.setFirstname(fname);
                user.setLastname(lname);
                user.setEmail_id(emailid);
                user.setMobile_number(mobilenumber);
                user.setPassword(pwd);
                user.setConfirmPassword(confirmpassword);

                if (fname.equals(""))
                    firstname.setError("Required");
                else if (lname.equals(""))
                    lastname.setError("Required");
                else if (emailid.equals(""))
                    email_id.setError("Required");
                else if (!emailid.contains("@"))
                    email_id.setError("Invalid emailid");
                else if (mobilenumber.equals(""))
                    mobile_number.setError("Required");
                else if (pwd.equals(""))
                    password.setError("Required");
                else if (confirmpassword.equals(""))
                    confirm_password.setError("Required");
                else if (!pwd.equals(confirmpassword))
                    Toast.makeText(UserSignUp.this, "Passwords are not matched.", Toast.LENGTH_SHORT).show();
                else {
                    firstname.setError(null);
                    lastname.setError(null);
                    email_id.setError(null);
                    mobile_number.setError(null);
                    password.setError(null);
                    confirm_password.setError(null);
                    dbAdapter = new DBAdapter(UserSignUp.this);
                    check_insert_data = dbAdapter.addUser(user);
                    if (check_insert_data == true) {
                        Toast.makeText(UserSignUp.this, "Registered successfully!", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(UserSignUp.this, Login.class);
                        startActivity(intent);
                        firstname.setText("");
                        lastname.setText("");
                        email_id.setText("");
                        mobile_number.setText("");
                        password.setText("");
                        confirm_password.setText("");
                    } else {
                        Toast.makeText(UserSignUp.this, "Registration failed", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }
}
