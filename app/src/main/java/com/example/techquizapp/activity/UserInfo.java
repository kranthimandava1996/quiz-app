package com.example.techquizapp.activity;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.techquizapp.R;
import com.example.techquizapp.db.DBAdapter;
import com.example.techquizapp.models.User;
import com.theartofdev.edmodo.cropper.CropImage;

import de.hdodenhof.circleimageview.CircleImageView;

public class UserInfo extends AppCompatActivity {
    TextView change_profile;
    private Uri imageUri;
    private CircleImageView profileImageView;
    TextView first_name;
    TextView last_name;
    TextView emailId;
    TextView mobile_no;
    String email_id;
    Button change_password;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.user_info);
        email_id = getIntent().getStringExtra("email_id");
        profileImageView = findViewById(R.id.profile_image);
        change_password = (Button) findViewById(R.id.change_password);
        first_name = (TextView) findViewById(R.id.firstname);
        last_name = (TextView) findViewById(R.id.lastname);
        mobile_no = (TextView) findViewById(R.id.phone_no);
        emailId = (TextView) findViewById(R.id.emailId);

        getUserInfo();
        profileImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CropImage.activity().setAspectRatio(1, 1).start(UserInfo.this);
            }
        });
        change_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(UserInfo.this, ChangePassword.class);
                intent.putExtra("email_id", email_id);
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE && resultCode == RESULT_OK && data != null) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            imageUri = result.getUri();
            profileImageView.setImageURI(imageUri);
            DBAdapter dbAdapter = new DBAdapter(UserInfo.this);
            Boolean check_updated_image = dbAdapter.updateImage(String.valueOf(imageUri), email_id);
            if (check_updated_image == true) {
                Toast.makeText(UserInfo.this, "Profile pic updated.", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(UserInfo.this, "Profile pic is not updated.", Toast.LENGTH_SHORT).show();
            }
        }
    }

    public void getUserInfo() {
        DBAdapter dbAdapter = new DBAdapter(UserInfo.this);
        User user = dbAdapter.getUserInfo(email_id);
        if (user.getImage() != null) {
            Log.d("UserInfo", "getUserInfo1");
            String file_path = user.getImage();
            imageUri = Uri.parse(file_path);
            profileImageView.setImageURI(imageUri);
        }
        else {
            Log.d("UserInfo", "getUserInfo2");
            String uri = "@drawable/ic_person";
            int imageResource = getResources().getIdentifier(uri, null, "com.example.techquizapp");
            Drawable res = getResources().getDrawable(imageResource);
            profileImageView.setImageDrawable(res);
        }
        String fname = user.getFirstname();
        String lname = user.getLastname();
        String mo_no = user.getMobile_number();
        first_name.setText(fname);
        last_name.setText(lname);
        emailId.setText(email_id);
        mobile_no.setText(mo_no);
    }
}
