package com.example.techquizapp.activity;

import android.graphics.Color;
import android.os.Bundle;
import android.view.Gravity;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.techquizapp.R;
import com.example.techquizapp.db.DBAdapter;
import com.example.techquizapp.models.QuestionResults;

import java.util.ArrayList;

public class QuizesScores extends AppCompatActivity {
    ArrayList<QuestionResults> list;
    String email_id;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.scores_list);
        TextView no_scores = (TextView) findViewById(R.id.no_scores);
        email_id = getIntent().getStringExtra("email_id");
        DBAdapter dbAdapter = new DBAdapter(QuizesScores.this);
        list = dbAdapter.getScores(email_id);
        if(list.size()>0) {
            tableInit();
        } else {
            no_scores.setText("You haven't participated quizes yet.");
        }
    }
    public void tableInit() {
        TableLayout tl = (TableLayout) findViewById(R.id.table_main);
        TableRow tbr0 = new TableRow(QuizesScores.this);

        TextView tv0 = new TextView(QuizesScores.this);
        tv0.setText(" Sl.No ");
        tv0.setTextColor(Color.WHITE);
        tbr0.addView(tv0);

        TextView tv1 = new TextView(QuizesScores.this);
        tv1.setText(" Date ");
        tv1.setTextColor(Color.WHITE);
        tbr0.addView(tv1);

        TextView tv2 = new TextView(QuizesScores.this);
        tv2.setText("Score");
        tv2.setTextColor(Color.WHITE);
        tbr0.addView(tv2);
        tl.addView(tbr0);
        for (int i=0; i<list.size(); i++) {
            QuestionResults questionResults = list.get(i);
            TableRow tableRow = new TableRow(QuizesScores.this);
            TextView _tv1 = new TextView(QuizesScores.this);
            _tv1.setText("" + i);
            _tv1.setTextColor(Color.WHITE);
            _tv1.setGravity(Gravity.CENTER);
            tableRow.addView(_tv1);

            TextView _tv2 = new TextView(QuizesScores.this);
            _tv2.setText(questionResults.getDate());
            _tv2.setTextColor(Color.WHITE);
            _tv2.setGravity(Gravity.CENTER);
            tableRow.addView(_tv2);

            TextView _tv3 = new TextView(QuizesScores.this);
            _tv3.setText(questionResults.getScore());
            _tv3.setTextColor(Color.WHITE);
            _tv3.setGravity(Gravity.CENTER);
            tableRow.addView(_tv3);
            tl.addView(tableRow);
        }

    }
}
