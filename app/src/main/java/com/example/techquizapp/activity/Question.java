package com.example.techquizapp.activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.example.techquizapp.R;
import com.example.techquizapp.db.DBAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.util.Date;

public class Question extends AppCompatActivity implements ResultDialog.DialogListener {
    JSONArray jArray;
    int i = 0;
    int x;
    int result = 0;
    RadioGroup radioGroup;
    TextView question;
    String email_id;
    RadioButton question_choice1;
    RadioButton question_choice2;
    RadioButton question_choice3;
    RadioButton question_choice4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.question);
        email_id = getIntent().getStringExtra("email_id");
        callingApi();
    }

    void callingApi() {
        question = (TextView) findViewById(R.id.question);
         question_choice1 = (RadioButton) findViewById(R.id.question_choice1);
         question_choice2 = (RadioButton) findViewById(R.id.question_choice2);
         question_choice3 = (RadioButton) findViewById(R.id.question_choice3);
         question_choice4 = (RadioButton) findViewById(R.id.question_choice4);

        radioGroup = (RadioGroup) findViewById(R.id.question_choice);
        String url = "https://quizapi.io/api/v1/questions?limit=10&apiKey=rRYOQEhHOeSQhICEhwOINsMtwaZCxJbjLRbteATC";
        //String url = "https://opentdb.com/api.php?amount=10";
        RequestQueue requestQueue = Volley.newRequestQueue(Question.this);
        JsonArrayRequest jsonRequest = new JsonArrayRequest(
                Request.Method.GET,
                url,
                null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        Log.d("rest response", response.toString());
                        try {

                            jArray = response;

                            JSONObject jsoObj = jArray.getJSONObject(0);
                            String question_1 = jsoObj.optString("question");
                            question.setText(question_1);


                            JSONObject answers = jsoObj.getJSONObject("answers");
                            String ot1 = answers.getString("answer_a");
                            String ot2 = answers.getString("answer_b");
                            String ot3 = answers.getString("answer_c");
                            String ot4 = answers.getString("answer_d");

                            question_choice1.setText(ot1);
                            question_choice2.setText(ot2);
                            question_choice3.setText(ot3);
                            question_choice4.setText(ot4);
                        } catch (JSONException E) {
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("error response", error.toString());
                    }
                });
        jsonRequest.setShouldCache(false);
        requestQueue.add(jsonRequest);

//        Button pr = (Button) findViewById(R.id.prvious_question);
//        pr.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                x--;
//                pre(x);
//            }
//
//            private void pre(int i) {
//                try {
//                    JSONObject jsoObj = jArray.getJSONObject(i);
//                    String question_1 = jsoObj.optString("question");
//                    question1.setText(question_1);
//
//
//                    JSONObject answers = jsoObj.getJSONObject("answers");
//                    //JSONArray opts = jsoObj.getJSONArray("incorrect_answers");
//                    String ot1 = answers.getString("answer_a");
//                    String ot2 = answers.getString("answer_b");
//                    String ot3 = answers.getString("answer_c");
//                    String ot4 = answers.getString("answer_d");
//
//                    question1_choice1.setText(ot1);
//                    question1_choice2.setText(ot2);
//                    question1_choice3.setText(ot3);
//                    question1_choice4.setText(ot4);
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//            }
//        });
        Button submit = (Button) findViewById(R.id.submit);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int selectedId = radioGroup.getCheckedRadioButtonId();
                if (selectedId == -1) {
                    Toast.makeText(Question.this,
                            "No answer has been selected.",
                            Toast.LENGTH_SHORT)
                            .show();
                } else {
                    RadioButton radioButton = (RadioButton) findViewById(selectedId);
                    try {
                        JSONObject jsoObj = jArray.getJSONObject(i);

                        String correct_ans_option = jsoObj.optString("correct_answer");
                        JSONObject answers = jsoObj.getJSONObject("answers");
                        String ans_option;
                        if (correct_ans_option != "null") {
                            ans_option = answers.getString(correct_ans_option);
                        } else {
                            correct_ans_option = "answer_b";
                            ans_option = answers.getString(correct_ans_option);
                        }

                        if (radioButton.getText() == ans_option) {
                            result++;
                            Toast.makeText(Question.this,
                                    "Correct answer", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(Question.this,
                                    "Wrong answer", Toast.LENGTH_SHORT).show();
                        }
                        i++;
                        if (i >= 9) {
                            openDialog();

                        } else {
                            radioGroup.clearCheck();
                            Thread.sleep(2500);
                            nextQuestion(i);
                        }
                    } catch (JSONException e) {
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }
    private void nextQuestion(int i) {
        try {
            JSONObject jsoObj = jArray.getJSONObject(i);
            String question_1 = jsoObj.optString("question");
            question.setText(question_1);


            JSONObject answers = jsoObj.getJSONObject("answers");
            //JSONArray opts = jsoObj.getJSONArray("incorrect_answers");
            String ot1 = answers.getString("answer_a");
            String ot2 = answers.getString("answer_b");
            String ot3 = answers.getString("answer_c");
            String ot4 = answers.getString("answer_d");

            question_choice1.setText(ot1);
            question_choice2.setText(ot2);
            question_choice3.setText(ot3);
            question_choice4.setText(ot4);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    public  void openDialog() {
        ResultDialog resultDialog = new ResultDialog();
        Bundle bundle = new Bundle();
        bundle.putString("final_result", String.valueOf(result));
        resultDialog.setArguments(bundle);
        resultDialog.show(getSupportFragmentManager(), "RESULT");
        try{
            DBAdapter dbAdapter= new DBAdapter(Question.this);
            String currentDateTime = DateFormat.getDateInstance().format(new Date());
            Boolean check_insert_data = dbAdapter.storeQuizResult(email_id, result, currentDateTime);
        }catch (Exception e) {
            Toast.makeText(Question.this, "Something went wrong", Toast.LENGTH_SHORT).show();;
        }
    }
    @Override
    public void applyTexts() {
        Intent intent = new Intent(Question.this, DashBoard.class);
        startActivity(intent);
    }
}
