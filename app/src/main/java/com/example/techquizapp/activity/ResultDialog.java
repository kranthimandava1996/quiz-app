package com.example.techquizapp.activity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDialogFragment;

import com.example.techquizapp.R;

public class ResultDialog extends AppCompatDialogFragment {
    private String result;
    private  TextView final_result;
    private DialogListener listener;

    @NonNull
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        super.onCreateDialog(savedInstanceState);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.result_dialog,null);
        builder.setView(view)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        listener.applyTexts();
                    }
                });
        final_result =  view.findViewById(R.id.final_result);
        Bundle data = getArguments();
        if(data != null) {
            result = data.getString("final_result");
        }
        final_result.setText(result);
        return builder.create();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
           listener = (DialogListener) context;
        }catch (ClassCastException e){
             throw new ClassCastException(context.toString() + "Need to implement DialogListener");
        }
    }

    public interface DialogListener {
        void applyTexts();
    }
}
