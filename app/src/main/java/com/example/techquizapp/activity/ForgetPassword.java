package com.example.techquizapp.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.techquizapp.R;
import com.example.techquizapp.db.DBAdapter;

public class ForgetPassword extends AppCompatActivity {

    EditText emailId;
    EditText new_password;
    EditText confirm_password;
    Button save;
    DBAdapter dbAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.forget_password);
        save = (Button) findViewById(R.id.fPasswod);

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                emailId = (EditText) findViewById(R.id.femail);
                new_password = (EditText) findViewById(R.id.fnewPassword);
                confirm_password = (EditText) findViewById(R.id.fconfirmPassword);


                String email_id = emailId.getText().toString().trim();
                String pwd = new_password.getText().toString().trim();
                String confirmpassword = confirm_password.getText().toString().trim();

                if (email_id.equals(""))
                    emailId.setError("Required");
                else if (!email_id.contains("@"))
                    emailId.setError("Invalid emailid");
                else if (pwd.equals(""))
                    new_password.setError("Required");
                else if (confirmpassword.equals(""))
                    confirm_password.setError("Required");
                else if (!pwd.equals(confirmpassword))
                    Toast.makeText(ForgetPassword.this, "Passwords are not matched.", Toast.LENGTH_SHORT).show();
                else {
                    emailId.setError(null);
                    new_password.setError(null);
                    confirm_password.setError(null);
                    try {
                        dbAdapter = new DBAdapter(ForgetPassword.this);
                        Boolean isUserExist = dbAdapter.isUserRegistered(email_id);
                        if (isUserExist == true) {
                            try {
                                dbAdapter = new DBAdapter(ForgetPassword.this);
                                Boolean isPasswordChanged = dbAdapter.forgetPassword(pwd, email_id);
                                if (isPasswordChanged == true) {
                                    Intent intent = new Intent(ForgetPassword.this, Login.class);
                                    startActivity(intent);
                                    emailId.setText("");
                                    new_password.setText("");
                                    confirm_password.setText("");
                                    Toast.makeText(ForgetPassword.this, "New password has been updated.", Toast.LENGTH_SHORT).show();
                                } else {
                                    Toast.makeText(ForgetPassword.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                                }
                            } catch (Exception e) {
                                Toast.makeText(ForgetPassword.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                                ;
                            }
                        } else {
                            Toast.makeText(ForgetPassword.this, "Unknown emailid", Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception e) {
                        Toast.makeText(ForgetPassword.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                        ;
                    }
                }
            }
        });

    }
}
