package com.example.techquizapp.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.techquizapp.R;
import com.google.android.gms.common.api.Status;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.AutocompleteActivity;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;

import java.util.Arrays;
import java.util.List;

public class PlacesAutoComplete extends AppCompatActivity {
    EditText search_place;
    TextView rs1;
    TextView rs2;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.places_autocomplete);
        search_place = (EditText) findViewById(R.id.search_place);
        rs1 = (TextView) findViewById(R.id.results1);
        rs2 = (TextView) findViewById(R.id.results2);

        //Initialize places
        Places.initialize(getApplicationContext(), "AIzaSyBy17SUjImynZwRITm4chOPLNdrJIGtWbE");
        search_place.setFocusable(false);
        search_place.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                 //Initialize the place field list
                 List<Place.Field> fieldList = Arrays.asList(Place.Field.ADDRESS, Place.Field.LAT_LNG,
                         Place.Field.NAME);
                 //Create intent
                Intent intent = new Autocomplete.IntentBuilder(AutocompleteActivityMode.OVERLAY,fieldList).build(PlacesAutoComplete.this);

                //Start activity result
                startActivityForResult(intent, 100);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == 100 && resultCode == RESULT_OK) {
            // When success, initialize place
            Place place = Autocomplete.getPlaceFromIntent(data);

            //Set address on edittext
            search_place.setText(place.getAddress());

            //Set locality name
            rs1.setText(String.format("Locality name: %s", place.getName()));

            //Set latitude & longitude
            rs2.setText(String.valueOf(place.getLatLng()));

        } else if(resultCode == AutocompleteActivity.RESULT_ERROR) {
            //When error, initialize status
            Status status = Autocomplete.getStatusFromIntent(data);
            Toast.makeText(getApplicationContext(), status.getStatusMessage(), Toast.LENGTH_SHORT).show();
        }
    }
}
