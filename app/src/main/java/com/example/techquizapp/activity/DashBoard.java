package com.example.techquizapp.activity;


import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import com.example.techquizapp.R;
import com.example.techquizapp.db.DBAdapter;
import com.example.techquizapp.models.QuestionResults;

import java.util.ArrayList;

public class DashBoard extends AppCompatActivity {
    String email_id;
    TextView total_quizes;
    TextView scores_percentage;
    ArrayList<QuestionResults> list;
    int sum = 0;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dashb);
        email_id = getIntent().getStringExtra("email_id");
        total_quizes =  findViewById(R.id.total_quizes);
        scores_percentage =  findViewById(R.id.scores_percentages);
        CardView card1 =  findViewById(R.id.card1);
        CardView card2 =  findViewById(R.id.card2);
        CardView card3 =  findViewById(R.id.card3);
        CardView card4 =  findViewById(R.id.card4);
        CardView card5 =  findViewById(R.id.card5);
        getScores();
        card1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(DashBoard.this, UserInfo.class);
                intent.putExtra("email_id", email_id);
                startActivity(intent);
            }
        });

        card2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(DashBoard.this, PlacesAutoComplete.class);
                startActivity(intent);
            }
        });

        card3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(DashBoard.this, Question.class);
                intent.putExtra("email_id", email_id);
                startActivity(intent);
            }
        });

        card4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(DashBoard.this, GoogleMaps.class);
                startActivity(intent);
            }
        });

        card5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(DashBoard.this, QuizesScores.class);
                intent.putExtra("email_id", email_id);
                startActivity(intent);
            }
        });
    }
    private void getScores() {
        DBAdapter dbAdapter = new DBAdapter(DashBoard.this);
        ArrayList<Integer> scoresList = new ArrayList<Integer>();
        list = dbAdapter.getScores(email_id);
       if(list.size()>0) {
           for (QuestionResults questionResults:list) {
               String score = questionResults.getScore();
               int _score = Integer.parseInt(score);
               scoresList.add(_score);
           }
           Log.d("scoresLists", String.valueOf(scoresList));
           for (int i = 0; i < scoresList.size(); i++) {
               sum += scoresList.get(i);
           }
           int scores_pecentage = (sum / scoresList.size())*10 ;
           total_quizes.setText("Total participated quizes are: " + scoresList.size());
           scores_percentage.setText("Scores percentage: " + scores_pecentage+"%");
       }
    }
}
