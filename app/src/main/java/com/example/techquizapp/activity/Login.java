package com.example.techquizapp.activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.techquizapp.R;
import com.example.techquizapp.db.DBAdapter;


public class Login extends AppCompatActivity{
    EditText username;
    EditText password;
    Boolean check_insert_data;
    Boolean isUserExist;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Button loginBtn = (Button) findViewById(R.id.loginBtn);
        TextView forgetPassword = (TextView) findViewById(R.id.forgetpassword);
        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                username = (EditText) findViewById(R.id.editTextUserName);
                password = (EditText) findViewById(R.id.editTextPassword);
                String uname = username.getText().toString().trim();
                String pwd = password.getText().toString().trim();
                Log.d("username", username.getText().toString());
                Log.d("password", password.getText().toString());
                if (uname.equals("")) {
                    username.setError("Invalid Username");
                } else if (pwd.equals("")) {
                    password.setError("Invalid Password");
                } else {

                    try{
                        DBAdapter dbAdapter= new DBAdapter(Login.this);
                        check_insert_data = dbAdapter.checkUserAuthentication(uname, pwd);
                        isUserExist = dbAdapter.isUserRegistered(uname);
                    }catch (Exception e) {
                        Toast.makeText(Login.this, "Something went wrong", Toast.LENGTH_SHORT).show();;
                    }
                    if(isUserExist) {
                        if(check_insert_data == true) {
                            Intent intent = new Intent(Login.this, DashBoard.class);
                            intent.putExtra("email_id", uname);
                            startActivity(intent);
                            username.setText("");
                            password.setText("");
                        }else {
                            Toast.makeText(Login.this, "Unauthorized", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(Login.this, "Not registered.", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
        forgetPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Login.this, ForgetPassword.class);
                startActivity(intent);
            }
        });
        

    }
}
