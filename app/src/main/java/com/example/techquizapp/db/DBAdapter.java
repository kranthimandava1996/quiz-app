package com.example.techquizapp.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.example.techquizapp.models.User;
import com.example.techquizapp.models.QuestionResults;
import java.util.ArrayList;


public class DBAdapter extends SQLiteOpenHelper {

    // All Static variables
    // Database Version
    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "QuizDb";

    // Contacts table name
    private static final String USER_INFO_TABLE = "user_table";
    private static final String QUIZ_RESULT = "quiz_result";

    private static final String KEY_ID = "id";
    private static final String KEY_FIRSTNAME = "firstname";
    private static final String KEY_LASTNAME = "lastname";
    private static final String KEY_EMAIL_ID = "email_id";
    private static final String KEY_MO_NO = "mobile_number";
    private static final String KEY_ADDRESS = "address";
    private static final String KEY_IMAGE = "image";
    private static final String KEY_PASSWORD = "password";
    private static final String KEY_DATE_TIME =  "date_time";
    private static final String KEY_SCORE = "score";

    public DBAdapter(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        String queryUser = "CREATE TABLE " + USER_INFO_TABLE + " (" +
                KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                KEY_FIRSTNAME + " TEXT, " +
                KEY_LASTNAME + " TEXT, " +
                KEY_EMAIL_ID + " TEXT, " +
                KEY_MO_NO + " TEXT, " +
                KEY_ADDRESS + " TEXT, " +
                KEY_IMAGE + " TEXT, " +
                KEY_PASSWORD + " TEXT" + ")";

        String queryUserQuizResult = "CREATE TABLE " + QUIZ_RESULT + " (" +
                KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                KEY_EMAIL_ID + " TEXT, " +
                KEY_DATE_TIME + " TEXT, " +
                KEY_SCORE + " INTEGER" + ")";

        Log.d("queryUser", queryUser);
        Log.d("queryUser", queryUserQuizResult);
        try {
            db.execSQL(queryUser);
            db.execSQL(queryUserQuizResult);
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("Exception", e.getMessage());
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int arg1, int arg2) {
        try {
            db.execSQL("DROP TABLE IF EXISTS " + USER_INFO_TABLE);
            db.execSQL("DROP TABLE IF EXISTS " + QUIZ_RESULT);
            onCreate(db);
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("Exception", e.getMessage());
        }
    }

    public Boolean addUser(User user) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        String firstname = user.getFirstname();
        String lastname = user.getLastname();
        String email_id = user.getEmail_id();
        String mobile_number = user.getMobile_number();
        String password = user.getPassword();
        contentValues.put("firstname", firstname);
        contentValues.put("lastname", lastname);
        contentValues.put("email_id", email_id);
        contentValues.put("mobile_number", mobile_number);
        //contentValues.put("address", "");
        contentValues.put("password", password);
        long result = db.insert(USER_INFO_TABLE, null, contentValues);
        db.close();
        if (result == -1) {
            return false;
        } else {
            return true;
        }
    }

    public Boolean storeQuizResult(String email_id, int score, String date_time) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(KEY_EMAIL_ID, email_id);
        contentValues.put(KEY_DATE_TIME, date_time);
        contentValues.put(KEY_SCORE, score);
        long result = db.insert(QUIZ_RESULT, null, contentValues);
        if (result == -1) {
            return false;
        } else {
            return true;
        }
    }

    public Boolean isUserRegistered(String user_name) {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery("select * from  user_table where email_id = ?", new String[]{user_name});
        if (cursor.getCount() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public Boolean checkUserAuthentication(String user_name, String password) {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery("select * from  user_table where email_id = ? and password = ?", new String[]{user_name, password});
        if (cursor.getCount() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public Boolean updateImage(String image_path, String email_id) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("image", image_path);
        int i = db.update(USER_INFO_TABLE, contentValues, "email_id = ?", new String[]{email_id});
        if (i > 0) {
            return true;
        } else {
            return false;
        }
    }

    public Boolean changePassword(String old_password, String new_password, String email_id) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("password", new_password);
        int i = db.update(USER_INFO_TABLE, contentValues, "email_id = ? and password = ?", new String[]{email_id, old_password});
        if (i > 0) {
            return true;
        } else {
            return false;
        }
    }
    public Boolean forgetPassword(String new_password, String email_id) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("password", new_password);
        int i = db.update(USER_INFO_TABLE, contentValues, "email_id = ?", new String[]{email_id});
        if (i > 0) {
            return true;
        } else {
            return false;
        }
    }
    public User getUserInfo(String email_id) {
        User user = new User();
        SQLiteDatabase db = this.getWritableDatabase();
        Log.d("DBAdapter", email_id);
        Cursor cursor = db.rawQuery("select * from  user_table where email_id = ? ", new String[]{email_id});
        if (cursor.moveToFirst()) {
            do {
                user.setFirstname(cursor.getString(1));
                user.setLastname(cursor.getString(2));
                user.setEmail_id(cursor.getString(3));
                user.setMobile_number(cursor.getString(4));
                //user.setAddress(cursor.getString(5));
                user.setImage(cursor.getString(6));
                user.setPassword(cursor.getString(7));

            } while (cursor.moveToNext());
        }
        Log.d("DBAdapter", user.getFirstname() + "  " + user.getEmail_id());
        return user;
    }

    public ArrayList<QuestionResults> getScores(String email_id) {
        ArrayList<QuestionResults> scoresList = new ArrayList<QuestionResults>();
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery("select * from  quiz_result where email_id = ? ", new String[]{email_id});
        if (cursor.moveToFirst()) {
            do {
                QuestionResults questionResults = new QuestionResults();
                questionResults.setEmail_id(cursor.getString(1));
                questionResults.setDate(cursor.getString(2));
                questionResults.setScore(cursor.getString(3));
                scoresList.add(questionResults);
            } while (cursor.moveToNext());
        }
        return scoresList;
    }
}