package com.example.techquizapp.models;

public class User {
    private  String firstname;
    private  String lastname;
    private  String mobile_number;
    private  String email_id;
    private  String image;
    private  String password;
    private  String confirm_password;



    public  String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public  String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public  String getEmail_id() {
        return email_id;
    }

    public void setEmail_id(String email_id) {
        this.email_id = email_id;
    }

    public  String getMobile_number() {
        return mobile_number;
    }

    public void setMobile_number(String mobile_number) {
        this.mobile_number = mobile_number;
    }

    public  String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public  String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    public void setConfirmPassword(String confirm_password) {
        this.confirm_password = confirm_password;
    }
    public  String getConfirmPassword() {
        return confirm_password;
    }
}
