package com.example.techquizapp.models;

import java.lang.reflect.Array;
public class Question {
    private  String category;
    private  String type;
    private  String difficulty;
    private  String question;
    private  Boolean correct_answer;
    private  Array incorrect_answers;

    public Question(String category, String type, String difficulty, String question, Boolean correct_answer,
                    Array incorrect_answers) {
            this.category = category;
            this.type = type;
            this.difficulty = difficulty;
            this.question = question;
            this.correct_answer = correct_answer;
            this.incorrect_answers = incorrect_answers;
    }
}
