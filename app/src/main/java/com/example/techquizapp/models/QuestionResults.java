package com.example.techquizapp.models;

public class QuestionResults {
    public String email_id;
    public String date_time;
    public String score;

    public String getEmail_id() {
        return email_id;
    }

    public void setEmail_id(String email_id) {
        this.email_id = email_id;
    }

    public String getDate() {
        return date_time;
    }

    public void setDate(String date) {
        this.date_time = date;
    }

    public String getScore() {
        return score;
    }

    public void setScore(String score) {
        this.score = score;
    }
}
